module gnome-git-stats

go 1.13

require (
	github.com/hashicorp/go-retryablehttp v0.6.7 // indirect
	github.com/pelletier/go-toml v1.8.0 // indirect
	github.com/peterbourgon/ff v1.7.0
	github.com/peterbourgon/ff/v3 v3.0.0
	github.com/urfave/cli v1.22.4
	github.com/urfave/cli/v2 v2.2.0
	github.com/xanzy/go-gitlab v0.37.0
	golang.org/x/net v0.0.0-20200904194848-62affa334b73 // indirect
	golang.org/x/oauth2 v0.0.0-20200902213428-5d25da1a8d43 // indirect
	golang.org/x/time v0.0.0-20200630173020-3af7569d3a1e // indirect
)
