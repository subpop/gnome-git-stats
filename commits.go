package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"text/tabwriter"
	"time"

	"github.com/xanzy/go-gitlab"
)

func runCommits(token string, since *time.Time) error {
	git, err := gitlab.NewClient(token, gitlab.WithBaseURL("https://gitlab.gnome.org/"))
	if err != nil {
		return err
	}

	searchOptions := &gitlab.SearchOptions{
		PerPage: 30,
		Page:    1,
	}

	var totalCommits int
	authors := make(map[string]bool)
	for {
		projects, response, err := git.Search.ProjectsByGroup("GNOME", "", searchOptions)
		if err != nil {
			return err
		}

		for _, project := range projects {
			commits, resp, err := git.Commits.ListCommits(project.ID, &gitlab.ListCommitsOptions{
				Since: since,
			})
			switch resp.StatusCode {
			case http.StatusOK:
				break
			case http.StatusNotFound:
				if err != nil {
					log.Println(err)
				}
				continue
			default:
				if err != nil {
					return fmt.Errorf("%v: %v", resp.Status, err)
				}
			}

			totalCommits += len(commits)

			for _, commit := range commits {
				authors[commit.AuthorName] = true
			}
		}

		if response.NextPage == 0 {
			break
		}

		searchOptions.Page = response.NextPage
	}

	w := tabwriter.NewWriter(os.Stdout, 0, 0, 2, ' ', 0)
	fmt.Fprintf(w, "COMMITS\t%v\n", totalCommits)
	fmt.Fprintf(w, "CONTRIBUTORS\t%v\n", len(authors))
	w.Flush()

	return nil
}
