% gnome-git-stats 8

# NAME

gnome-git-stats - generate useful statistics for release notes

# SYNOPSIS

gnome-git-stats

```
[--help|-h]
```

**Usage**:

```
gnome-git-stats [GLOBAL OPTIONS] command [COMMAND OPTIONS] [ARGUMENTS...]
```

# GLOBAL OPTIONS

**--help, -h**: show help


# COMMANDS

## commits

compute total commits and contributors

**--since**="": only include commits since `DATE`

**--token**="": personal access token

## translations

print a list of languages with translation >= 80%

**--release**="": release set (i.e. "gnome-3-38")

## help, h

Shows a list of commands or help for one command

