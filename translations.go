package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"sort"
)

func runTranslations(release string) error {
	type statistic struct {
		LangName   string `json:"lang_name"`
		LangLocale string `json:"lang_locale"`
		UI         struct {
			TranslatedPerc int `json:"translated_perc"`
		} `json:"ui"`
	}

	res, err := http.Get(fmt.Sprintf("https://l10n.gnome.org/api/v1/releases/%v/stats", release))
	if err != nil {
		return err
	}
	defer res.Body.Close()

	var stats struct {
		Statistics []statistic `json:"statistics"`
	}
	decoder := json.NewDecoder(res.Body)
	if err := decoder.Decode(&stats); err != nil {
		return err
	}

	var languages []string
	for _, s := range stats.Statistics {
		if s.UI.TranslatedPerc >= 80 {
			languages = append(languages, s.LangName)
		}
	}

	sort.Sort(sort.StringSlice(languages))

	for _, language := range languages {
		fmt.Printf("    <item><p>%v</p></item>\n", language)
	}

	return nil
}
