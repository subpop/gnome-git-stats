package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli/v2"
)

func main() {
	app := &cli.App{
		Usage: "generate useful statistics for release notes",
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:   "generate-markdown",
				Hidden: true,
			},
		},
		Commands: []*cli.Command{
			{
				Name:  "commits",
				Usage: "compute total commits and contributors",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "token",
						Usage:    "personal access token",
						Required: true,
						EnvVars:  []string{"TOKEN"},
					},
					&cli.TimestampFlag{
						Name:     "since",
						Usage:    "only include commits since `DATE`",
						Required: true,
						Layout:   "2006-01-02",
					},
				},
				Action: func(c *cli.Context) error {
					return runCommits(c.String("token"), c.Timestamp("since"))
				},
			},
			{
				Name:  "translations",
				Usage: "print a list of languages with translation >= 80%",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "release",
						Usage:    `release set (i.e. "gnome-3-38")`,
						Required: true,
					},
				},
				Action: func(c *cli.Context) error {
					return runTranslations(c.String("release"))
				},
			},
		},
		Action: func(c *cli.Context) error {
			if c.Bool("generate-markdown") {
				data, err := c.App.ToMarkdown()
				if err != nil {
					return err
				}
				fmt.Println(data)
				return nil
			}
			cli.ShowAppHelpAndExit(c, 0)
			return nil
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
